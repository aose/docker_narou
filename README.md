Docker narou
====

小説家になろう のコンテンツをmobiに変換してKindleで読むためのDockerコンテナ。

## Description
* [Narou.rb](https://github.com/whiteleaf7/narou)
* Ruby + JDK
* AozoraEpub3
* Kindlegen

## Requirement
Docker環境があればだいたいいけるハズ。

## Usage
初回実行はInstall参照

    $ ./run.sh
    $ ./run.sh d n4706cp

## Install
Docker入ってる前提です。

    $ git clone https://bitbucket.org/aose/docker_narou
    $ cd docker_narou
    $ docker build -t narou .

初回のみ./run.shでinitが走ります。

（startup.sh内の設定が反映されるので、必要ならコンテナをビルドする前に編集する）

    $ ./run.sh

## Author

[i_aose](https://twitter.com/i_aose)