#!/bin/bash

docker run --rm -i -t \
	-v `pwd`/narou:/narou \
	-v `pwd`/mobi:/mobi \
	narou $*
