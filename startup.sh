#!/bin/sh

cd /narou

# setup narou.rb
if [ ! -e /narou/.narou ]; then
	narou init
	narou setting device=kindle
	narou setting convert.no-open=true
	narou setting convert.copy_to=/mobi
	narou setting download.wait-steps=10
	narou setting download.interval=0.7
	narou setting update.interval=3.0
	exit 0
fi

if [ $# -gt 0 ]; then
	/usr/local/bundle/bin/narou $*
else
	/usr/local/bundle/bin/narou update
fi
