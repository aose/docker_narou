FROM ruby:2.3.3-alpine

# install OpenJDK
# see https://github.com/docker-library/openjdk/tree/master/8-jdk/alpine
RUN { \
		echo '#!/bin/sh'; \
		echo 'set -e'; \
		echo; \
		echo 'dirname "$(dirname "$(readlink -f "$(which javac || which java)")")"'; \
	} > /usr/local/bin/docker-java-home \
	&& chmod +x /usr/local/bin/docker-java-home
ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk/jre
ENV PATH $PATH:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin
ENV JAVA_VERSION 8u111
ENV JAVA_ALPINE_VERSION 8.111.14-r0
RUN set -x \
	&& apk add --no-cache \
		openjdk8-jre="$JAVA_ALPINE_VERSION" \
	&& [ "$JAVA_HOME" = "$(docker-java-home)" ]

# install narou.rb
ENV LANG ja_JP.UTF-8
RUN gem install narou && \
    gem update narou

# setup aozoraepub
COPY AozoraEpub3-1.1.0b46.zip /AozoraEpub3-1.1.0b46.zip
RUN cd / && \
    mkdir aozoraepub && \
    unzip -d aozoraepub AozoraEpub3-1.1.0b46.zip && \
    rm -rf AozoraEpub3-1.1.0b46.zip

# add kindlegen
RUN mkdir kindlegen_work && \
    cd kindlegen_work && \
    wget "http://kindlegen.s3.amazonaws.com/kindlegen_linux_2.6_i386_v2_9.tar.gz" && \
    tar xvf kindlegen_linux_2.6_i386_v2_9.tar.gz && \
    chmod +x kindlegen && \
    mv kindlegen /aozoraepub/kindlegen && \
    cd .. && \
    rm -rf kindlegen_work

RUN mkdir -p /root/.narousetting/
COPY global_setting.yaml /root/.narousetting/global_setting.yaml
COPY startup.sh /startup.sh

VOLUME [ "/narou" ]
VOLUME [ "/mobi" ]
WORKDIR /

ENTRYPOINT [ "/bin/sh", "/startup.sh" ]
